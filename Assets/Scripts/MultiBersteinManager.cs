﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiBersteinManager : MonoBehaviour {

    public List<Vector3> originalPoints_A;
    public List<Vector3> originalPoints_B;

    private List<Vector3> OP_A;
    private List<Vector3> OP_B;

    [Range(5, 200)]
    public int Ndiscret;

    private LineRenderer originalLine_A;
    private LineRenderer bersteinLine_A;
    private LineRenderer originalLine_B;
    private LineRenderer bersteinLine_B;

    private float movementTrigger = 0.005f;

    // Use this for initialization
    void Start()
    {
        originalLine_A = transform.GetChild(0).GetComponent<LineRenderer>();
        bersteinLine_A = transform.GetChild(1).GetComponent<LineRenderer>();
        originalLine_B = transform.GetChild(2).GetComponent<LineRenderer>();
        bersteinLine_B = transform.GetChild(3).GetComponent<LineRenderer>();

        originalLine_A.material.SetColor("_EmissionColor", Color.cyan);
        bersteinLine_A.material.SetColor("_EmissionColor", Color.blue);
        originalLine_B.material.SetColor("_EmissionColor", Color.magenta);
        bersteinLine_B.material.SetColor("_EmissionColor", Color.red);

        InitPoints();
    }

    // Update is called once per frame
    void Update()
    {
        Points();
        OriginalLines();
        BersteinLines();
    }


    void InitPoints()
    {
        if (originalPoints_A.Count >= 2 && originalPoints_B.Count >= 1)
        {
            //Could be better
            originalPoints_A[originalPoints_A.Count - 1] = (originalPoints_A[originalPoints_A.Count - 2] + originalPoints_B[0]) / 2;
        }

        OP_A = new List<Vector3>(originalPoints_A);
        OP_B = new List<Vector3>(originalPoints_B);
    }

    void Points()
    {
        if(originalPoints_A.Count != OP_A.Count || originalPoints_B.Count != OP_B.Count)
        {
            InitPoints();
            return;
        }

        if(originalPoints_A.Count >= 2)
        {
            if (Mathf.Abs((originalPoints_A[originalPoints_A.Count - 1] - OP_A[OP_A.Count - 1]).magnitude) > movementTrigger)
            {
                Vector3 movement = originalPoints_A[originalPoints_A.Count - 1] - OP_A[OP_A.Count - 1];

                originalPoints_A[originalPoints_A.Count - 2] += movement;
                originalPoints_B[0] += movement;
            }
            else if(Mathf.Abs((originalPoints_A[originalPoints_A.Count - 2] - OP_A[OP_A.Count - 2]).magnitude) > movementTrigger)
            {
                // !!! DOES NOT WORK IF DISTANCE BETWEEN POINTS ARE NOT EQUALS
                Vector3 movement = originalPoints_A[originalPoints_A.Count - 2] - OP_A[OP_A.Count - 2];
                originalPoints_B[0] -= movement;
            }
            else if (Mathf.Abs((originalPoints_B[0] - OP_B[0]).magnitude) > movementTrigger)
            {
                // !!! DOES NOT WORK IF DISTANCE BETWEEN POINTS ARE NOT EQUALS
                Vector3 movement = originalPoints_B[0] - OP_B[0];
                originalPoints_A[originalPoints_A.Count - 2] -= movement;
            } 
        }

        OP_A = new List<Vector3>(originalPoints_A);
        OP_B = new List<Vector3>(originalPoints_B);
    }

    void OriginalLines()
    {
        int n = 0;
        originalLine_A.positionCount = OP_A.Count;
        foreach (Vector3 position in OP_A)
        {
            originalLine_A.SetPosition(n, position);
            ++n;
        }

        
        n = 0;
        if(OP_A.Count >= 1)
        {
            originalLine_B.positionCount = OP_B.Count + 1;
            originalLine_B.SetPosition(0, OP_A[OP_A.Count - 1]);
            n = 1;
        }
        else
        {
            originalLine_B.positionCount = OP_B.Count;
        }

        foreach (Vector3 position in OP_B)
        {
            originalLine_B.SetPosition(n, position);
            ++n;
        }
    }

    void BersteinLines()
    {
        bersteinLine_A.positionCount = Ndiscret + 1;
        bersteinLine_B.positionCount = Ndiscret + 1;

        int n = 0;
        foreach (Vector3 position in Berstein(OP_A))
        {
            bersteinLine_A.SetPosition(n, position);
            ++n;
        }

        List<Vector3> B = new List<Vector3>();
        B.Add(OP_A[OP_A.Count - 1]);
        B.AddRange(OP_B);
        n = 0;
        foreach (Vector3 position in Berstein(B))
        {
            bersteinLine_B.SetPosition(n, position);
            ++n;
        }
    }


    List<Vector3> Berstein(List<Vector3> points)
    {
        List<Vector3> curve = new List<Vector3>();


        List<float> Bcoef = new List<float>();

        float nFact = 1;
        for (int i = 2; i <= points.Count - 1; ++i)
            nFact *= i;


        for (int i = 0; i < points.Count; ++i)
        {
            float iFact = 1;
            for (int j = 2; j <= i; ++j)
                iFact *= j;

            float inFact = 1;
            for (int j = 2; j <= points.Count - 1 - i; ++j)
                inFact *= j;

            float coef = nFact / (iFact * inFact);


            Bcoef.Add(coef);
        }
        

        for (int d = 0; d <= Ndiscret; ++d)
        {
            Vector3 position = new Vector3();
            float u = d / (float)Ndiscret;
            
            for (int i = 0; i < points.Count; ++i)
                position += Bcoef[i] * points[i] * Mathf.Pow(u, i) * Mathf.Pow(1 - u, points.Count - 1 - i);

            curve.Add(position);
        }

        return curve;
    }

}
