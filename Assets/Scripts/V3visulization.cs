﻿using UnityEditor;
using System.Reflection;
using UnityEngine;

[InitializeOnLoad]
class V3visulization
{
    static V3visulization()
    {
        SceneView.onSceneGUIDelegate += OnSceneGUI;
    }

    static void OnSceneGUI(SceneView scnView)
    {
        GameObject selObj;
        Vector3 v3;
        MonoBehaviour[] scripts;

        selObj = Selection.activeGameObject;
        if (selObj != null)
        {
            scripts = selObj.GetComponents<MonoBehaviour>();
            for (int i = 0; i < scripts.Length; i++)
            {
                MonoBehaviour data = scripts[i];
                FieldInfo[] fields = data.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
                for (int j = 0; j < fields.Length; j++)
                {
                    if (fields[j].GetValue(scripts[i]).GetType() == typeof(Vector3))
                    {
                        v3 = (Vector3)fields[j].GetValue(scripts[i]);
                        v3 = Handles.PositionHandle(v3, Quaternion.identity);
                        fields[j].SetValue(scripts[i], v3);
                    }
                }
            }
        }
    }
}
